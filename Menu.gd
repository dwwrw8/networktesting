extends Control

func _on_StartServer_pressed():
	NetworkManager.create_server()
	
	get_tree().change_scene("res://Game.tscn")

func _on_JoinServer_pressed():
	NetworkManager.connect_to_server()
	
#	get_tree().change_scene("res://Game.tscn")

func _on_Name_text_changed(new_text):
	NetworkManager.my_data["name"] = new_text if new_text != "" else "Anonymous"


func _on_IPAddress_text_changed(new_text):
	NetworkManager.server_ip = new_text if new_text != "" else NetworkManager.DEFAULT_IP

func _on_Port_text_changed(new_text):
	NetworkManager.server_port = int(new_text) if new_text != "" else NetworkManager.DEFAULT_PORT
