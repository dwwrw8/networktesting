extends Sprite


const SPEED = 200

var nickname = "UNDEFINED"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = nickname
	set_process_unhandled_input(is_network_master())


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_network_master():
		var x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
		var y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
		self.position += Vector2(x, y) * SPEED * delta
		rpc('update_position', position)

# 'puppet' means that this func can only be called by the network master
puppet func update_position(new_pos):
	position = new_pos

func set_nick(val):
	nickname = val
	if get_node_or_null("Label"):
		$Label.text = val
