extends Node
const DEFAULT_IP = "127.0.0.1"
const DEFAULT_PORT = 23456
const DEFAULT_MAX_PLAYERS = 8
const SERVER_ID = 1

var server_ip : String = DEFAULT_IP
var server_port : int = DEFAULT_PORT

var players := {}
var my_data := {
	"name": "Anonymous"
}

# Start a server from this instance
func create_server():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(server_port, DEFAULT_MAX_PLAYERS)
	get_tree().set_network_peer(peer)
	players[SERVER_ID] = my_data

# Connect to an external server
func connect_to_server():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(server_ip, server_port)
	get_tree().set_network_peer(peer)

# This just connects signals for later use
func _ready():
	# Network signals.
# warning-ignore:return_value_discarded
	get_tree().connect('connected_to_server', self, '_on_connected_to_server')
# warning-ignore:return_value_discarded
	get_tree().connect('network_peer_connected', self, '_on_player_connected')
# warning-ignore:return_value_discarded
	get_tree().connect('network_peer_disconnected', self, '_on_player_disconnected')

# Called by client, tells the server to add our data
func _on_connected_to_server():
	Notifications.notify("Connected to server")
	rpc_id(1, "register_player", my_data)

func _on_player_connected(other_player_id):
	Notifications.notify("Player %s Connected" % str(other_player_id))

func _on_player_disconnected(other_player_id):
	Notifications.notify("Player %s Disonnected" % str(other_player_id))
	players.erase(other_player_id)
	rpc("update_players", players)

# 'master' means this func can only run on the network master (i.e. server)
# BUT that means it is called (requested) by a non-master peer
master func register_player(data):
	players[get_tree().get_rpc_sender_id()] = data
	rpc_id(get_tree().get_rpc_sender_id(), "update_players", players)

# 'puppet' means that this is run on the non-master clients
# This means it is called (requested) by the server/master
puppet func update_players(data):
	players = data
	get_tree().change_scene("res://Game.tscn")
