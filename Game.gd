extends Node2D



# Called when the node enters the scene tree for the first time.
func _ready():
	# other players
	print(NetworkManager.players)
	for id in NetworkManager.players.keys():
		create_player(id, NetworkManager.players[id]['name'])
	
	# me
#	create_player(get_tree().get_network_unique_id(), NetworkManager.my_data['name'])
	rpc("create_player", get_tree().get_network_unique_id(), NetworkManager.my_data['name'])
	# Network signals.
# warning-ignore:return_value_discarded
	get_tree().connect('network_peer_disconnected', self, '_on_player_disconnected')
## warning-ignore:return_value_discarded
#	get_tree().connect('network_peer_connected', self, '_on_player_connected')
## warning-ignore:return_value_discarded
#	get_tree().connect('connected_to_server', self, '_on_connected_to_server')

remote func create_player(id, nick):
	var player = preload("res://Player.tscn").instance()
	player.set_network_master(id)
	player.name = str(id)
	player.set_nick(nick)
	add_child(player)

func _on_player_disconnected(other_player_id):
	get_node(str(other_player_id)).queue_free()
